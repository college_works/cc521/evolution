import random
import numpy as np
import math

from gray_code import to_binary, from_binary

encoder = to_binary
decoder = from_binary

"""
PROBLEMA:
min |a+ 2*b + 3*c + 4*d - 30|
a, b, c, d  in Z
"""


##################  DETERMINACION DE INTERVALO DE fenotipo
u = 5 # poblacion

x0 = 0 # cota inferior
x1 = 128 # cota superior

delta = 1

N = math.ceil(math.log2((x1-x0)/delta)) # numeros de bits para representar elementos de poblacion


print(f"u: {u}, a: {x0}, b: {x1}, delta: {delta}, nbits: {N}")


def fitness_func(p: str):
	#p: elemento de poblacion codificado (depende de la codificacion)
	f = lambda a, b, c, d: abs(a+ 2*b + 3*c + 4*d - 30) # funcion de fitness
	
	aa, bb, cc, dd = to_fenotipo(p)

	return f(aa, bb, cc, dd)

def to_cromosoma(p: str):
	a, b, c, d = p[0:N], p[N:2*N], p[2*N:3*N], p[3*N:4*N]
	aa = decoder(a)
	bb = decoder(b)
	cc = decoder(c)
	dd = decoder(d)
	
	return [aa, bb, cc, dd]

def to_fenotipo(p: str)-> float: 
	#p: elemento de poblacion codificado (depende de la codificacion)
	# a: fenotipo del primer elemento de la codificacion
	
	return [x0 + xx*delta for xx in to_cromosoma(p)]


def seleccion(poblacion, k: int = 10, s: float = 2.0):
	#import pdb; pdb.set_trace()
	punctuations = [(x, fitness_func(x)) for x in poblacion]
	sorted_punctuations = sorted(punctuations, key=lambda x: -x[1]) # rank = index of element
	
	prob_link = lambda rank, s: (2-s)/N + 2*rank*(s-1)/(N*(N-1))  
	_weights = [(x, prob_link(rank, s)) for rank, (x, fx)  in enumerate(sorted_punctuations)]

	pop = [x for x, w in _weights]
	weights = [w for x, w in _weights]

	selected = random.choices(pop, weights=weights, k=k)
	return selected

"""
	selected = []
	#import pdb; pdb.set_trace()
	for kk in range(k):
		r = random.random()


		i = 0
		#print(f"Selection {kk}")
		#import pdb; pdb.set_trace()
		while r > acc[i] and i < len(poblacion)-1:
			#print(f"r: {r}, acc[{i}]: {acc[i]}, i: {i}")
			i = i+1

		#print(f"selected: {i}")
		selected.append(poblacion[i])
		
	#print(selected)

	return selected
"""

def cruz_mutacion(selection, prob_muta: float = 0.5, prob_cruz: float = 0.1):
	## cruzamiento y mutacion
	
	ss = selection
	for i, s in enumerate(ss):
		r = random.random()
		## mutacion
		

		if r < prob_muta:
			# inidices a mutar 
			i0 = random.randint(0, N-1)
			i1 = random.randint(0, N-1)
			
			cromo = list(s)
			tmp = cromo[i0]
			cromo[i0] = cromo[i1]
			cromo[i1] = tmp
			selection[i] = int(''.join(cromo), 2)
			
		if r < prob_cruz:

			parent1 = list(s)
			# seleccionde otro padre para cruzamiento
			parent2 = list(random.choice(selection))
			
			# indice de corte
			i = random.randint(0, N-1)
			
			parent1_low = parent1[:i]
			parent1_high = parent1[i:]
			
			
			parent2_low = parent2[:i]
			parent2_high = parent2[i:]
			
			child1 = parent1_low + parent2_high
			child2 = parent1_high + parent2_low
			
			child1 = int(''.join(child1), 2)
			child2 = int(''.join(child2), 2)
			
			#import pdb; pdb.set_trace()
			selection = selection + [child1, child2]
			
	return selected
	
	
 
################## GENERACION DE POBLACION INICIAL
poblacion = [''.join([encoder(random.randint(0, 2**N-1), nbits=N) for i in range(4)]) for _ in range(u)]

print(f"pob: {poblacion}")


ITERATIONS = 10
k = 3 # elemets to be selected in each iteration

#import matplotlib.pyplot as plt


for iter in range(ITERATIONS):
	#
	selected = seleccion(poblacion, k)
	poblacion = cruz_mutacion(selected + poblacion)
	poblacion = seleccion(poblacion, u)

	#import pdb; pdb.set_trace()
	#print(f"[Iter {iter}] pob: {poblacion}")

	# get element with best fitness
	fitness = [(x, fitness_func(x)) for x in poblacion]
	
	best, f_best = max(fitness, key=lambda x: x[1])
	worse, f_worse  = max(fitness, key=lambda x: -x[1])
	
	
	print(f"iter: {iter}, best: {to_cromosoma(best)}, fitness: {f_best}")
	print(f"iter: {iter}, worse: {to_cromosoma(worse)}, fitness: {f_worse}")
	print("===================================")
	
	"""
	# plot solution
	if iter % 1000:
		x = [to_fenotipo(p) for p in poblacion]
		y = fitness
		plt.scatter(x, y)
		
		x = np.linspace(a, b, 1000)
		y = fitness_func(x)
		plt.plot(x, y)

		plt.show()
	"""	
