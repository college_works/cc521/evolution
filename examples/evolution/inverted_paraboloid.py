#!/usr/bin/env python3
#
# Demostrate how to find the global maximum of an inverted paraboloid using evolutionary algorithms
#
# [PROBLEM]
# max f(x, y) = 10 - x**2 - y**2  where x in [-1, 1] and y in [-2, 2]
#
# EXPECTED SOLUTION: (x, y) = (0, 0) and max f(x, y) = 10
import math

# class that join all (selection strategy, variation streategies, population) and evolve to find a solution to the problem
from evolution import Evolution

from evolution import PopulationGenerator, Individual
from evolution.fitness import Fitness

# selection strategy
from evolution.selection import ProportionalToFitness

# encoding
from evolution.encoding import IntervalPartition, MultipleIntervalEncoder
from evolution.encoding import BinaryGenEncoder

# variation operators
from evolution.variation.mutation import BitFlipMutation
from evolution.variation.crossover import OnePointCrossover

# defining our fitness function for this problem
class InvertedParaboloidFitness(Fitness):
    def compute(self, individual: Individual) -> float:
        x, y = individual.phenotype
        return 10 - x**2 - y**2

## defining our domain
partitions = [
        IntervalPartition(a=-1, b=1, delta=10**-10, gen_encoder_cls=BinaryGenEncoder), # x variable
        IntervalPartition(a=-2, b=2, delta=10**-10, gen_encoder_cls=BinaryGenEncoder), # y variable
        ]
domain_encoder = MultipleIntervalEncoder(partitions = partitions)

# defining our population generator (to generate a random population on each evolution)
population_generator = PopulationGenerator(domain_encoder=domain_encoder)


## Ensambling the Evolution class
class MyEvolution(Evolution):
    def __init__(self, population_generator: PopulationGenerator):
        fitness = InvertedParaboloidFitness()
        crossover_prob = 0.9
        mutation_prob = 0.1


        super(MyEvolution, self).__init__(
                population_generator=population_generator,
                fitness=fitness,
                selection_strategy=ProportionalToFitness(fitness),
                crossover_strategy=OnePointCrossover(),
                mutation_strategy=BitFlipMutation(),
                crossover_prob=crossover_prob,
                mutation_prob=mutation_prob)

    def converged(self) -> bool:
        # is not necesary to define a check_convergence funcion, but if you have enough information to define one,
        # just override the default check_convergence that just return False
        fitness = self.population_fitness()
        max_fitness = max(fitness)
        MAX_VALUE=10
        return math.isclose(MAX_VALUE, max_fitness, abs_tol=10**-3)


evol = MyEvolution(population_generator)
evol.evolve(population_size = 10,
            max_generations=512,  # maximum number of iteration (or generations)
            selection = 50, # number of parents selected from current population for reproduction
            descendents = 80, # number of children generated from reproduction process (crossover + mutation)
            evolutions = 3 # number of evolutions to perform
)
