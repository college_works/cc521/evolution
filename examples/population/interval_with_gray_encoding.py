#!/usr/bin/env python3
#
# Generate a population for a interval domain
#
# [DOMAIN]
# partition: (interval: [-5, 5], delta: 10**-5)
# population: 10

from evolution import Population
from evolution.encoding import  IntervalEncoder, GrayGenEncoder

N = 10
domain_encoder = IntervalEncoder(a=-5, b=5, delta=10**-5, gen_encoder_cls=GrayGenEncoder)
print(f"Domain encoder: {domain_encoder.describe}")

population = Population(population=N, encoder=domain_encoder)
print(f"Population size: {population.size}")
print(f"Population individuals: \n{population.individuals}")
