#!/usr/bin/env python3
#
# Demostrate how to encode an interval using binary gen encoding
#
# [EXAMPLE]
# interval: [-5, 5]
# initial delta: 10**-5

from evolution.encoding import IntervalEncoder, IntervalPartition
from evolution.encoding import BinaryGenEncoder, to_binary
from evolution.encoding import Chromosome, ChromosomeComponent, Gen

delta = 10**-5
partition = IntervalPartition(a=-5, b=5, delta=delta, gen_encoder_cls=BinaryGenEncoder)
domain_encoder = IntervalEncoder(partition=partition)

print(f"Interval: [{domain_encoder.lower_bound}, {domain_encoder.upper_bound}], delta: {delta}")
print(f"Required Bits: {domain_encoder.nbits}")
print(f"Recomputed delta: {domain_encoder.delta}")

def create_chromosome(bitstream: str) -> Chromosome:
    gen_encoder = domain_encoder.gen_encoder
    gen = gen_encoder.encode(bitstream)
    component = ChromosomeComponent(gen, encoder=gen_encoder)
    return Chromosome(components=[component])

# decoding low bound of interval
x = create_chromosome(to_binary(0, nbits=domain_encoder.nbits))
print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}") # decode method give the phenotype associate to a genotype (encoding of phenotype)

# encoding value on interval
x = create_chromosome("1011".zfill(domain_encoder.nbits))
print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")

# decoding upper bound of interval
x = create_chromosome(to_binary(2**domain_encoder.nbits-1, nbits=domain_encoder.nbits))
print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")

# generate random point on interval
x = domain_encoder.random()
print(f"[RANDOM] chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")
print(f"[RANDOM - WITH DECODING] chromosome: {x.to_str(decode=True)} <> phenotype: {domain_encoder.decode(x)}")
