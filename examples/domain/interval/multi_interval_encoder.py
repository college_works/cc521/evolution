#!/usr/bin/env python3
#
# Demostrate how to encode a domain composed of multiple intervals 
# using binary encoding for the first gen and gray encoding for the second one.
#
# [EXAMPLE]
# partitions: 
# - interval=[-5, 5], delta=10**-5, gen-encoder: Binary
# - interval=[-10, 10], delta=10**-7, gen-encoder: Gray
#
# For more information about gen encoding (BinaryGenEncoder and GrayGenEncoder) and interval encoding (IntervalEncoder),
# check the following examples:
# - examples/domain/interval/single/binary_gen_encoding.py
# - examples/domain/interval/single/gray_gen_encoding.py

from evolution.encoding import MultipleIntervalEncoder, IntervalPartition
from evolution.encoding import GenEncoder, BinaryGenEncoder, GrayGenEncoder
from evolution.encoding import to_binary, to_gray # decimal to other system (e.g. binary or gray codes) codification function
from evolution.encoding import Chromosome, ChromosomeComponent

def create_component(bitstream: str, gen_encoder: GenEncoder) -> ChromosomeComponent:
    gen = gen_encoder.encode(bitstream)
    component = ChromosomeComponent(gen, encoder=gen_encoder)
    return component

partitions = [
    IntervalPartition(a=-5, b=5, delta=10**-5, gen_encoder_cls=BinaryGenEncoder), 
    IntervalPartition(a=-10, b=10, delta=10**-7, gen_encoder_cls=GrayGenEncoder)
]

domain_encoder = MultipleIntervalEncoder(partitions=partitions)
encoder1, encoder2 = domain_encoder.interval_encoders

print(f"Domain encoder description: {domain_encoder.describe}")

# decoding low bound of interval
total_bits = sum([encoder.nbits for encoder in domain_encoder.interval_encoders])
x = Chromosome(components = [
        create_component(to_binary(0, nbits=encoder1.nbits), gen_encoder=encoder1.gen_encoder),
        create_component(to_gray(0, nbits=encoder2.nbits), gen_encoder=encoder2.gen_encoder)
    ])
print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")

# encoding value on interval
x = Chromosome(components = [
        create_component("1011".zfill(encoder1.nbits), gen_encoder=encoder1.gen_encoder),
        create_component("0011".zfill(encoder2.nbits), gen_encoder=encoder2.gen_encoder)
    ])

print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")
print(f"chromosome: {x} <> genes: {x.genes}")

# decoding upper bound of interval
x = Chromosome(components = [
        create_component(to_binary(2**encoder1.nbits-1, nbits=encoder1.nbits), gen_encoder=encoder1.gen_encoder),
        create_component(to_gray(2**encoder2.nbits-1, nbits=encoder2.nbits), gen_encoder=encoder2.gen_encoder)
    ])

print(f"chromosome: {x} <> phenotype: {domain_encoder.decode(x)}")

# random point
x = domain_encoder.random()
print(f"[RANDOM] chromosome: {x} <> genes: {x.genes} <> phenotype: {domain_encoder.decode(x)}")
