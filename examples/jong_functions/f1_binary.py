#!/usr/bin/env python3
#
# [PROBLEM]
# min F1(x1, x2, x3) = x1**2 + x2**2 + x3**2  ,where xi in [-1, 1]
#
# EXPECTED SOLUTION: (x1, x2, x3) = (0, 0, 0) and min f(x1, x2, x3) = 0
import math

# class that join all (selection strategy, variation streategies, population) and evolve to find a solution to the problem
from evolution import Evolution

from evolution import Population, Individual
from evolution.fitness import Fitness

# selection strategy
from evolution.selection import ProportionalToInverseFitness

# encoding
from evolution.encoding import IntervalPartition, MultipleIntervalEncoder
from evolution.encoding import BinaryGenEncoder

# variation operators
from evolution.variation.mutation import BitFlipMutation
from evolution.variation.crossover import OnePointCrossover

# defining our fitness function for this problem
class F1(Fitness):
    def compute(self, individual: Individual) -> float:
        x1, x2, x3 = individual.phenotype
        return x1**2 + x2**2 + x3**2

## defining our domain
partitions = [
        IntervalPartition(a=-1, b=1, delta=10**-10, gen_encoder_cls=BinaryGenEncoder), # x1 variable
        IntervalPartition(a=-1, b=1, delta=10**-10, gen_encoder_cls=BinaryGenEncoder), # x2 variable
        IntervalPartition(a=-1, b=1, delta=10**-10, gen_encoder_cls=BinaryGenEncoder), # x3 variable
    ]
domain_encoder = MultipleIntervalEncoder(partitions = partitions)

## Initializing our population (using the created domain encoder)
N = 100 # number of individual on population
population = Population(domain_encoder=domain_encoder, population=N)


## Ensambling the Evolution class
class MyEvolution(Evolution):
    def __init__(self, population: Population):
        fitness = F1()
        crossover_prob = 0.9
        mutation_prob = 0.1


        super(MyEvolution, self).__init__(
                population=population,
                fitness=fitness,
                selection_strategy=ProportionalToInverseFitness(fitness),
                crossover_strategy=OnePointCrossover(),
                mutation_strategy=BitFlipMutation(),
                crossover_prob=crossover_prob,
                mutation_prob=mutation_prob)

    def converged(self) -> bool:
        # is not necesary to define a check_convergence funcion, but if you have enough information to define one,
        # just override the default check_convergence that just return False
        fitness = self.population_fitness()
        TARGET_VALUE=10
        return math.isclose(TARGET_VALUE, min(fitness), abs_tol=10**-3)


evol = MyEvolution(population)
evol.evolve(max_generations=128,  # maximum number of iteration (or generations)
            selection = 50, # number of parents selected from current population for reproduction
            descendents = 30) # number of children generated from reproduction process (crossover + mutation)
