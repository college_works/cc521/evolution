#!/usr/bin/env python3
#
# [PROBLEM]
# min F1(x1, x2, x3, x4, x5) = sum(math.floor(xi) for xi in [x1, x2, x3, x4, x5]) ,where xi in [-10, 10]
#
# EXPECTED SOLUTION: ?
import math

# class that join all (selection strategy, variation streategies, population) and evolve to find a solution to the problem
from evolution import Evolution 

from evolution import Population, Individual
from evolution.fitness import Fitness

# selection strategy
from evolution.selection import ProportionalToInverseFitness

# encoding
from evolution.encoding import IntervalPartition, MultipleIntervalEncoder
from evolution.encoding import GrayGenEncoder

# variation operators
from evolution.variation.mutation import BitFlipMutation
from evolution.variation.crossover import OnePointCrossover

# defining our fitness function for this problem
class F3(Fitness):
    def compute(self, individual: Individual) -> float:
        x = individual.phenotype
        return sum(math.floor(xi) for xi in x) + 51.0 # a constant is added to avoid division by zero

## defining our domain
partitions = [
        IntervalPartition(a=-10, b=10, delta=10**-12, gen_encoder_cls=GrayGenEncoder), # x1 variable
        IntervalPartition(a=-10, b=10, delta=10**-12, gen_encoder_cls=GrayGenEncoder), # x2 variable
        IntervalPartition(a=-10, b=10, delta=10**-12, gen_encoder_cls=GrayGenEncoder), # x3 variable
        IntervalPartition(a=-10, b=10, delta=10**-12, gen_encoder_cls=GrayGenEncoder), # x4 variable
        IntervalPartition(a=-10, b=10, delta=10**-12, gen_encoder_cls=GrayGenEncoder), # x5 variable
    ]
domain_encoder = MultipleIntervalEncoder(partitions = partitions)

## Initializing our population (using the created domain encoder)
N = 100 # number of individual on population
population = Population(domain_encoder=domain_encoder, population=N)


## Ensambling the Evolution class
class MyEvolution(Evolution):
    def __init__(self, population: Population):
        fitness = F3()
        crossover_prob = 0.9
        mutation_prob = 0.2


        super(MyEvolution, self).__init__(
                population=population,
                fitness=fitness,
                # this selection strategy was selected because this is a minimization problem
                selection_strategy=ProportionalToInverseFitness(fitness), 
                crossover_strategy=OnePointCrossover(),
                mutation_strategy=BitFlipMutation(),
                crossover_prob=crossover_prob,
                mutation_prob=mutation_prob)

    def converged(self) -> bool:
        # is not necesary to define a check_convergence funcion, but if you have enough information to define one,
        # just override the default check_convergence that just return False
        return False


evol = MyEvolution(population)
evol.evolve(max_generations=128,  # maximum number of iteration (or generations)
            selection = 70, # number of parents selected from current population for reproduction
            descendents = 50) # number of children generated from reproduction process (crossover + mutation)
