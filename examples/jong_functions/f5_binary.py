#!/usr/bin/env python3
#
# [PROBLEM]
# min F4(X) = sum(i*xi**4) for xi in X) + GAUSS(0 1) ,where xi in [-2, 2]
#
# EXPECTED SOLUTION: x = (0, 0, 0, ..., 0) and min F4(X) = 0
import math
import random
import numpy as np

# class that join all (selection strategy, variation streategies, population) and evolve to find a solution to the problem
from evolution import Evolution 

from evolution import Population, Individual
from evolution.fitness import Fitness

# selection strategy
from evolution.selection import ProportionalToInverseFitness

# encoding
from evolution.encoding import IntervalPartition, MultipleIntervalEncoder
from evolution.encoding import BinaryGenEncoder

# variation operators
from evolution.variation.mutation import BitFlipMutation
from evolution.variation.crossover import OnePointCrossover

# defining our fitness function for this problem
class F5(Fitness):
    def __init__(self, K: float, A: np.array, c: np.array):
        """dim(A) = (2, 25), dim(c) = (25, 1)"""
        self.K = K
        self.A = A
        self.c = c

    def compute(self, individual: Individual) -> float:
        x = individual.phenotype
        f = lambda x, j: self.c[j] + sum((x[i] - self.A[i, j])**6 for i in range(2))
       
        #import pdb; pdb.set_trace()
        S = sum(1.0/f(x, j) for j in range(25))

        invF = 1/self.K + S

        return 1.0/invF

## defining our domain
partitions = [
        IntervalPartition(a=-2, b=2, delta=10**-10, gen_encoder_cls=BinaryGenEncoder),
        IntervalPartition(a=-2, b=2, delta=10**-10, gen_encoder_cls=BinaryGenEncoder)
    ]
domain_encoder = MultipleIntervalEncoder(partitions = partitions)

## Initializing our population (using the created domain encoder)
N = 200 # number of individual on population
population = Population(domain_encoder=domain_encoder, population=N)


## Ensambling the Evolution class
class MyEvolution(Evolution):
    def __init__(self, population: Population):
        A = np.ones((2, 25))
        c = np.ones(25)
        K = 1
        fitness = F5(K, A, c)

        crossover_prob = 0.9
        mutation_prob = 0.7


        super(MyEvolution, self).__init__(
                population=population,
                fitness=fitness,
                # this selection strategy was selected because this is a minimization problem
                selection_strategy=ProportionalToInverseFitness(fitness), 
                crossover_strategy=OnePointCrossover(),
                mutation_strategy=BitFlipMutation(),
                crossover_prob=crossover_prob,
                mutation_prob=mutation_prob)

    def converged(self) -> bool:
        # is not necesary to define a check_convergence funcion, but if you have enough information to define one,
        # just override the default check_convergence that just return False
        return False
        #fitness = self.population_fitness()
        #TARGET_VALUE=0.0
        #return math.isclose(TARGET_VALUE, min(fitness), abs_tol=10**-3)


evol = MyEvolution(population)
evol.evolve(max_generations=128,  # maximum number of iteration (or generations)
            selection = 100, # number of parents selected from current population for reproduction
            descendents = 100) # number of children generated from reproduction process (crossover + mutation)
