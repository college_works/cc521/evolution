## mutation strategies for genes encoded as bit stream (sequence of bits)
from .bit import BitFlipMutation

## base class
from .base import MutationStrategy
