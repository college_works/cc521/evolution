## crossover strategied for gen encoded as bit stream (sequence of bits)
from .bit import OnePointCrossover

## base class
from .base import CrossoverStrategy
