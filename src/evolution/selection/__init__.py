from .proportional2fitness import ProportionalToFitness
from .proportional2inversefitness import ProportionalToInverseFitness

## base classes
from .base import SelectionStrategy
